define([
    "dojo/_base/declare",
    "mxui/widget/_WidgetBase",

    "mxui/dom",
    "dojo/dom",
    "dojo/dom-prop",
    "dojo/dom-geometry",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-construct",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/text",
    "dojo/html",
    "dojo/_base/event",


], function (declare, _WidgetBase, dom, dojoDom, dojoProp, dojoGeometry, dojoClass, dojoStyle, dojoConstruct, dojoArray, lang, dojoText, dojoHtml, dojoEvent) {
    "use strict";

    return declare("ChangeBrowserAddress.widget.ChangeBrowserAddress", [ _WidgetBase ], {


        // Internal variables.
        _handles: null,
        _contextObj: null,

        // Parameters configured in the Modeler.
        browserAddress: "",

        constructor: function () {
            this._handles = [];
        },

        postCreate: function () {
        },

        update: function (obj, callback) {
            this._contextObj = obj;
            this._updateRendering(callback);
        },

        resize: function (box) {
        },

        uninitialize: function () {
        },

        _updateRendering: function (callback) {

            let evaluatedBrowserAddress = this._getEvaluatedString(this.browserAddress);
            mx.ui.getContentForm().url = evaluatedBrowserAddress;

            this._executeCallback(callback, "_updateRendering");
        },

        _getEvaluatedString: function (label) {
            var replaceFunction = lang.hitch(this, function (replaceString, firstMatch, secondMatch) {
            	if (this._contextObj){
	                var attributeValue = this._contextObj.get(secondMatch).toLowerCase();;
	                return replaceString.replace(firstMatch, attributeValue);
                }
                return replaceString;
            });

            var expression = /({(.+?)})/g;
            return label.replace(expression, replaceFunction);
        },

        _isEmptyString: function (str) {
            return (!str || 0 === str.trim().length);
        },

        // Shorthand for running a microflow
        _execMf: function (mf, guid, cb) {
            logger.debug(this.id + "._execMf");
            if (mf && guid) {
                mx.ui.action(mf, {
                    params: {
                        applyto: "selection",
                        guids: [guid]
                    },
                    callback: lang.hitch(this, function (objs) {
                        if (cb && typeof cb === "function") {
                            cb(objs);
                        }
                    }),
                    error: function (error) {
                        console.debug(error.description);
                    }
                }, this);
            }
        },

        // Shorthand for executing a callback, adds logging to your inspector
        _executeCallback: function (cb, from) {
            if (cb && typeof cb === "function") {
                cb();
            }
        }
    });
});

require(["ChangeBrowserAddress/widget/ChangeBrowserAddress"]);